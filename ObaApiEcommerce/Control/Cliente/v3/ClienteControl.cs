﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Control.Cliente.v3
{
    public class ClienteControl
    {

        public Domain.Cliente.v3.Cliente cadastrar (Domain.Cliente.v3.Cliente  cliente)
        {
            return new Dao.Cliente.v3.ClienteDao().CadastrarCliente(cliente);
        }


        public void UpdateOptin(Domain.Cliente.v3.UpdateOptinOptout updateOptinOptout)
        {
            new Dao.Cliente.v3.ClienteDao().UpdateOptin(updateOptinOptout);
        }

        public Domain.Cliente.v3.Cliente buscar(string email)
        {
            Domain.Cliente.v3.Cliente cliente = new Dao.Cliente.v3.ClienteDao().buscaCliente(email);
            return cliente;
        }

    }
}
