﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Control.Cliente.v2
{
    public class ClienteControl
    {

        public Domain.Cliente.v1.Cliente cadastrar (Domain.Cliente.v1.Cliente  cliente)
        {
            try
            {
                return new Dao.Cliente.v2.ClienteDao().CadastrarCliente(cliente);
            }catch(Exception e)
            {
                throw e;
            }

        }


        public void inativarCliente(string  pr_nrocgccpf, string pr_digcgccpf)
        {
            new Dao.Cliente.v2.ClienteDao().inativarCliente(pr_nrocgccpf, pr_digcgccpf);
        }

        public Domain.Cliente.v1.Cliente buscar(string email)

        {
            try
            {
                Domain.Cliente.v1.Cliente cliente = new Dao.Cliente.v2.ClienteDao().buscaCliente(email);

                return cliente;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
