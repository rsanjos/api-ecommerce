﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;

namespace ObaApiEcommerce.Utilitarios.DataBase
{
    public class Connection : IDisposable
    {
        public static IConfiguration Configuration { get; set; }
        private OracleConnection OraConnection;
        public const string CON_SP = "C5SPConnection";
        public const string CON_DF = "C5DFConnection";
        public const string DW = "DWConnection";
        public const string MC = "MCConnection";

        public Connection(string dataBase)
        {
            try
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

                Configuration = builder.Build();

                string connectionString = Configuration["DataBase:ConnectionStrings:" + dataBase + ""];

                OraConnection = new OracleConnection(connectionString);

                if (OraConnection.State == ConnectionState.Closed)
                {
                    OraConnection.Open();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void getResultfromCommand(OracleCommand comand)
        {
            try
            {
                comand.CommandType = CommandType.StoredProcedure;
                comand.Connection = OraConnection;
                comand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Dispose()
        {
            if (OraConnection != null)
            {
                OraConnection.Dispose();
                OraConnection = null;
            }
        }
    }
}
