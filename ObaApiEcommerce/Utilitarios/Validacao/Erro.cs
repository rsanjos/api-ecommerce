﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Utilitarios.Validacao
{
    public class Erro
    {
        public  string propriedade { get; set; }
        public string mensagem    { get; set; }
    }
}
