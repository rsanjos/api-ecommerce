﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Utilitarios.ObjectConvert
{
    public class ReadSQLToObject
    {
        public List<Object> DataReaderList(IDataReader dr, Type t)
        {
            List<object> list = new List<object>();
            var cols = dr.GetSchemaTable().Rows.Cast<DataRow>().Select(row => row["ColumnName"].ToString().ToUpper() as string).ToList();
            while (dr.Read())
            {
                Object auxObj = Activator.CreateInstance(t);
                foreach (PropertyInfo prop in auxObj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
                {
                    try
                    {
                        if (cols.Contains(prop.Name.ToUpper()))
                        {
                            if (!object.Equals(dr[prop.Name.ToUpper()], DBNull.Value))
                            {
                                prop.SetValue(auxObj, Convert.ChangeType((dr[prop.Name]), prop.PropertyType), null);

                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
                list.Add(auxObj);
            }
            return list;
        }

        public Object DataReaderObject(IDataReader dr, Type t)
        {

            Object obj = Activator.CreateInstance(t);
            var cols = dr.GetSchemaTable().Rows.Cast<DataRow>().Select(row => row["ColumnName"].ToString().ToUpper() as string).ToList();
            while (dr.Read())
            {
                PropertyInfo[] props = obj.GetType().GetProperties(BindingFlags.Instance|BindingFlags.NonPublic|BindingFlags.Public);
                foreach (PropertyInfo prop in props)
                {
                    if (prop.Name.ToUpper().Equals("ORIGEM")){

                       var T = dr[prop.Name];
                    }

                    if (cols.Contains(prop.Name.ToUpper()))
                    {
                        
                        try
                        {
                            if (!object.Equals(dr[prop.Name.ToUpper()], DBNull.Value))
                            {                                 
                                prop.SetValue(obj, Convert.ChangeType((dr[prop.Name]), prop.PropertyType), null);
                            }
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                }
            }
            return obj;
        }

        public Object DataReaderObject(IDataReader dr, Object obj)
        {

            var cols = dr.GetSchemaTable().Rows.Cast<DataRow>().Select(row => row["ColumnName"].ToString().ToUpper() as string).ToList();
            while (dr.Read())
            {
                PropertyInfo[] props = obj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                foreach (PropertyInfo prop in props)
                {
                    if (cols.Contains(prop.Name.ToUpper()))
                    {
                        prop.SetValue(obj, Convert.ChangeType((dr[prop.Name]), prop.PropertyType), null);
                    }
                }
            }
            return obj;
        }
    }
}
