﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ObaApiEcommerce.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class SecurityController : ApiController
    {

        private IConfiguration _config;
        public SecurityController(IConfiguration Configuration)
        {
            _config = Configuration;
        }

        [HttpPost]
        [Authorize("Bearer")]
        public IActionResult GerarTokenJWT()
        {
            try
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtToken:SecretKey"]));
                //var token = new JwtSecurityToken(issuer: issuer, audience: audience, expires: DateTime.Now.AddMinutes(120), signingCredentials: credentials);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    //Expires = DateTime.UtcNow.AddYears(10),
                    SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var stringToken = tokenHandler.WriteToken(token);
                Envelope.Data = stringToken;
                return Ok(Envelope);
            }
            catch (Exception ex)
            {
                Envelope.Message = ex.Message;
                return BadRequest(ex.Message);
            }
        }
    }
}
