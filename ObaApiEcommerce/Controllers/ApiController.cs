﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ObaApiEcommerce.Controllers
{
    public class ApiController : ControllerBase
    {
        protected BaseResponse Envelope { get; }

        public ApiController()
        {
            Envelope = new BaseResponse();
        }
    }

    public class BaseResponse
    {
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
