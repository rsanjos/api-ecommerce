﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ObaApiEcommerce.Controllers.Produto.v1
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromocaoController : ApiController
    {
        /// <summary>
        /// Busca Promoções vigentes
        /// </summary>
        /// <param name="nroEmpresa"> Obrigatório, filtro unico</param>
        /// <returns>Retorna as promoções vigentes para empresa informada.</returns>
        [HttpGet("{nroEmpresa}")]
        [Authorize("Bearer")]
        [Produces(typeof(Domain.Produto.v1.Promocao))]
        public IActionResult Get(long nroEmpresa )
        {
            try
            {
                Envelope.Data = new ObaApiEcommerce.Domain.Produto.v1.Promocao().buscaPromocao(nroEmpresa);

                return Ok(Envelope);

            }
            catch (Exception ex)
            {
                Envelope.Message = ex.Message;
                return BadRequest(ex.Message);
            }
        }
    }
}
