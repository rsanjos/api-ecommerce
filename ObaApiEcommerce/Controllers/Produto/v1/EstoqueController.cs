﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ObaApiEcommerce.Controllers.Produto.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EstoqueController : ApiController
    {
        /// <summary>
        /// Busca estoque disponível para o produto.
        /// </summary>
        /// <param name="seqProduto"> Obrigatório, identificador do produto.</param>
        /// <param name="nroEmpresa"> Obrigatório, identificador da empresa.</param>
        /// <returns> Retorna o estoque disponivel para o produto e a empresa informados.</returns>
        /// 
        [HttpGet("{seqProduto}, {nroEmpresa}")]
        [Authorize("Bearer")]
        [Produces(typeof(List<Domain.Produto.v1.EstoqueProduto>))]

        public IActionResult Get(long seqProduto, int nroEmpresa )
        {
            try
            {
                Envelope.Data = new ObaApiEcommerce.Domain.Produto.v1.EstoqueProduto().buscaEstoque(seqProduto, nroEmpresa);
                return Ok(Envelope);
            }
            catch (Exception ex)
            {
                Envelope.Message = ex.Message;
                return BadRequest(ex.Message);
            }
        }
    }
}
