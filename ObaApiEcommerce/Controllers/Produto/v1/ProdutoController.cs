﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ObaApiEcommerce.Domain.Produto.v1;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ObaApiEcommerce.Controllers.Produto.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProdutoController : ApiController
    {

        /// <summary>
        /// Busca produtos com alteração a partir da data informada.
        /// </summary>
        /// <param name="dataBase"> Data de corte, formato 'YYYY-MM-DD HH24:mi:ss'</param>
        /// <param name="nroEmpresa">Numero da empresa de fitro</param>
        /// <returns> Retorna a lista de produtos</returns>
        [HttpGet("{dataBase},{nroEmpresa}")]
        [Authorize("Bearer")]
        [Produces(typeof(List<Domain.Produto.v1.Produto>))]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Domain.Cliente.v1.Endereco>))]
        public IActionResult Get(DateTime dataBase, int nroEmpresa)
        {
            try
            {
                Envelope.Data = new ObaApiEcommerce.Domain.Produto.v1.Produto().buscaProduto(dataBase, nroEmpresa);

                return Ok(Envelope);
            }
            catch (Exception ex)
            {
                Envelope.Message = ex.Message;
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Busca o produto informado.
        /// </summary>
        /// <param name="seqProduto"> Id único do produto.</param>
        /// <param name="nroEmpresa">Numero da empresa de fitro.</param>
        /// <returns> Retorna o produto informado.</returns>
        [HttpPut("{seqProduto},{nroEmpresa}")]
        [Authorize("Bearer")]
        [Produces(typeof(Domain.Produto.v1.Produto))]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Domain.Cliente.v1.Endereco>))]
        public IActionResult GetProduto(long  seqProduto, int nroEmpresa)
        {
            try
            {
                Envelope.Data = new ObaApiEcommerce.Domain.Produto.v1.Produto().buscaProduto(seqProduto, nroEmpresa);

                return Ok(Envelope);
            }
            catch (Exception ex)
            {
                Envelope.Message = ex.Message;
                return BadRequest(ex.Message);
            }
        }
    }
}
