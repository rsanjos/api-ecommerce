﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ObaApiEcommerce.Controllers.Produto.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class infoNutricionalController : ApiController
    {
        /// <summary>
        /// Busca informações nutricionais de produtos.
        /// </summary>
        /// <param name="seqProduto"> Obrigatório, filtro único.</param>
        /// <returns> Retorna as informações nutricionais do produto informado.</returns>
        /// 
        [HttpGet("{seqProduto}")]
        [Authorize("Bearer")]
        [Produces(typeof(Domain.Produto.v1.Nutricionais))]
        public IActionResult Get(long seqProduto)
        {
            try
            {
                Envelope.Data = new ObaApiEcommerce.Domain.Produto.v1.Nutricionais().buscaNutricionais(seqProduto);

                return Ok(Envelope);

            }
            catch (Exception ex)
            {
                Envelope.Message = ex.Message;
                return BadRequest(ex.Message);
            }
        }
    }
}
