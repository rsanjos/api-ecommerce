﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ObaApiEcommerce.Controllers.Pedido.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PedidoController : ApiController
    {
        // POST api/<PedidoController>
        [HttpPost]
        [Authorize("Bearer")]
        [Produces(typeof(Domain.Pedido.v1.Pedido))]
        public IActionResult Post(Domain.Pedido.v1.Pedido pedido)
        {
            try
            {
                Envelope.Data = pedido.gravaPedido(pedido);                
                Envelope.Message = pedido.erros.Count  == 0 ?"Sucesso" : "Falha"; 
                return Ok(pedido);
            }
            catch (Exception e)
            {
                Envelope.Message = e.Message;
                return BadRequest(e);
            }
        }

        /// <summary>
        ///  Consulta pedidos já enviados ao erp.
        /// </summary>
        /// <param name="codSistemaParceiro"></param>
        /// <param name="nroPedidoAfv"></param>
        /// <returns></returns>

        [HttpGet("{codSistemaParceiro},{nroPedidoAfv}")]
        [Authorize("Bearer")]
        [Produces(typeof(Domain.Pedido.v1.Pedido))]
        public IActionResult Get(long codSistemaParceiro, long nroPedidoAfv)
        {
            try
            {
                Envelope.Data = ObaApiEcommerce.Domain.Pedido.v1.Pedido.buscaPedido(codSistemaParceiro, nroPedidoAfv);
                return Ok(Envelope);
            }
            catch (Exception e)
            {
                Envelope.Message = e.Message;
                return BadRequest(e);
            }
        }

    }
}
