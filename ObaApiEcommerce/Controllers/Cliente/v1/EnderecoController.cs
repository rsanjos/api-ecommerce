﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ObaApiEcommerce.Domain.Cliente.v1;

//For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ObaApiEcommerce.Controllers.Cliente.v1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EnderecoController : ApiController
    {
        // GET: api/<EnderecoController>
        /// <summary>
        ///  Lista os endereços cadastrados para o cliente.
        /// </summary>
        /// <param name="seqPessoa"> Código sequencial do cliente.</param>
        /// <returns>Retorna a lista de endereços do cliente.</returns>
        [HttpGet("{seqPessoa}")]
        [Authorize("Bearer")]
        [Produces(typeof(List<Domain.Cliente.v1.Endereco>))]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Domain.Cliente.v1.Endereco>))]
        public IActionResult Get(long seqPessoa)
        {
            try
            {
                Envelope.Data = new Endereco().buscar(seqPessoa);
                return Ok(Envelope);

            }
            catch (Exception e)
            {
                Envelope.Message = e.Message;
                return BadRequest(e);
            }
        }

        /// <summary>
        ///  Cadastra ou atualiza o endereço informado.
        /// </summary>
        /// <param name="endereco"></param>
        /// <returns> Retorna a lista de endereços do cliente.</returns>
        [HttpPost]
        [Authorize("Bearer")]
        [Produces(typeof(List<Domain.Cliente.v1.Endereco>))]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(List<Domain.Cliente.v1.Endereco>))]
        public IActionResult Post(Domain.Cliente.v1.Endereco endereco)
        {
            try
            {
                Envelope.Data = new Endereco().Cadastrar(endereco);
                return Ok(Envelope);

            }
            catch (Exception e)
            {
                Envelope.Message = e.Message;
                return BadRequest(e);
            }
        }
    }
}
