﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ObaApiEcommerce.Controllers.Cliente.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
#pragma warning disable CS1591 // O comentário XML ausente não foi encontrado para o tipo ou membro visível publicamente
    public class ClienteController : ApiController
#pragma warning restore CS1591 // O comentário XML ausente não foi encontrado para o tipo ou membro visível publicamente
    {
        /// <summary>
        /// Cadastra ou atualiza os dados de clientes.
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns>Retorna o cliente cadastrado ou atualizado.</returns>
        [HttpPost]
        [Authorize("Bearer")]
        [Produces(typeof(Domain.Cliente.v1.Cliente))]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(Domain.Cliente.v1.Cliente))]
        public IActionResult Post(Domain.Cliente.v1.Cliente cliente)
        {
            try
            {
                Envelope.Data = cliente.cadastrar(cliente);
                return Ok(cliente);

            }
            catch (Exception e)
            {
                Envelope.Message = e.Message;
                return BadRequest(e);
            }
        }
        /// <summary>
        ///  Pesquisa cliente por e-mail ou cpf.
        /// </summary>
        /// <param name="valor">E-mail ou cpf do cliente</param>
        /// <returns> Retorna somente um registro</returns>
        [HttpGet("{valor}")]
        [Authorize("Bearer")]
        [Produces(typeof(Domain.Cliente.v1.Cliente))]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(Domain.Cliente.v1.Cliente))]
        public IActionResult Get(string valor)
        {
            try
            {
                Envelope.Data = new Domain.Cliente.v1.Cliente().buscar(valor);
                return Ok(Envelope);

            }
            catch (Exception e)
            {
                Envelope.Message = e.Message;
                return BadRequest(e);
            }
        }

    }
}
