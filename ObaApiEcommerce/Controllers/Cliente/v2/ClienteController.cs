﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Controllers.Cliente.v2
{
    [ApiController]
    [Route("api/v2/[controller]")]
    public class ClienteController : ApiController
    {
        /// <summary>
        /// Cadastra ou atualiza os dados de clientes.
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns>Retorna o cliente cadastrado ou atualizado.</returns>
        [HttpPost]
        [Authorize("Bearer")]
        //[Produces(typeof(Domain.Cliente.v2.Cliente))]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(Domain.Cliente.v1.Cliente))]
        public IActionResult Post(Domain.Cliente.v1.Cliente cliente)
        {
            try
            {
                Envelope.Data = new ObaApiEcommerce.Control.Cliente.v2.ClienteControl().cadastrar(cliente);
                return Ok(Envelope);

            }
            catch (Exception e)
            {
                //Envelope.Message = e.Message;
                return BadRequest(e);
            }
        }
       
        /// <summary>
        ///  Pesquisa cliente por e-mail ou cpf.
        /// </summary>
        /// <param name="valor">E-mail ou cpf do cliente</param>
        /// <returns> Retorna somente um registro</returns>
        [HttpGet("{valor}")]
        [Authorize("Bearer")]
        //[SwaggerResponse(HttpStatusCode.OK, Type = typeof(Domain.Cliente.v1.Cliente))]
        public IActionResult Get(string valor)
        {
            try
            {
                Envelope.Data = new  Control.Cliente.v2.ClienteControl().buscar(valor);
                return Ok(Envelope);

            }
            catch (Exception e)
            {
                //Envelope.Message = e.Message;
                return BadRequest();
            }
        }

       
        [HttpPost("inativarCliente/{nrocgccpf}-{digcgccpf}")]
        [Authorize("Bearer")]
        public IActionResult inativarCliente(string nrocgccpf, string digcgccpf)
        {
            try
            {
                
                new ObaApiEcommerce.Control.Cliente.v2.ClienteControl().inativarCliente(nrocgccpf, digcgccpf);
                Envelope.Message = "01(um) registro foi localizado para os dados informado.";
                return Ok(Envelope);
            }
            catch(Exception e)
            {
                return BadRequest();
            }
        }
    }
}
