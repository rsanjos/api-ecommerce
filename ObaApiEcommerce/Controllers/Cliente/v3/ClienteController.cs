﻿using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ObaApiEcommerce.Domain.Cliente.v3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Controllers.Cliente.v3
{
    [ApiController]
    [Route("api/v3/[controller]")]
    public class ClienteController : ApiController
    {
        private readonly TelemetryClient _telemetry;

        public ClienteController(TelemetryClient telemetry)
        {
            _telemetry = telemetry;
        }

        /// <summary>
        /// Cadastra ou atualiza os dados de clientes.
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns>Retorna o cliente cadastrado ou atualizado.</returns>
        [HttpPost]
        [Authorize("Bearer")]
        public IActionResult Post(Domain.Cliente.v3.Cliente cliente)
        {
            try
            {
                Envelope.Data = new ObaApiEcommerce.Control.Cliente.v3.ClienteControl().cadastrar(cliente);
                _telemetry.TrackEvent("[SUCCESS] POST /cliente/v3",
                                new Dictionary<string, string> {
                                    { "cpf", $"{String.Format("{0:D9}", cliente.cgccpf)}-{String.Format("{0:D2}", cliente.digcgccpf)}" },
                                    { "email", $"{cliente.email}" }
                                }
                );
                return Ok(Envelope);

            }
            catch (Exception e)
            {
                _telemetry.TrackEvent("[ERROR] POST /cliente/v3",
                                new Dictionary<string, string> {
                                    { "cpf", $"{String.Format("{0:D9}", cliente.cgccpf)}-{String.Format("{0:D2}", cliente.digcgccpf)}" },
                                    { "email", $"{cliente.email}" },
                                    { "exception", e.Message },
                                    { "stackTrace", e.StackTrace }
                                }
                );
                return BadRequest(e);
            }
        }
       
        /// <summary>
        ///  Pesquisa cliente por e-mail ou cpf.
        /// </summary>
        /// <param name="valor">E-mail ou cpf do cliente</param>
        /// <returns> Retorna somente um registro</returns>
        [HttpGet("{valor}")]
        [Authorize("Bearer")]
        public IActionResult Get(string valor)
        {
            try
            {
                Envelope.Data = new Control.Cliente.v3.ClienteControl().buscar(valor);
                _telemetry.TrackEvent("[SUCCESS] GET /cliente/v3",
                    new Dictionary<string, string> {
                        { "valor", valor }
                    }
                );
                return Ok(Envelope);

            }
            catch (Exception e)
            {
                _telemetry.TrackEvent("[ERROR] GET /cliente/v3",
                    new Dictionary<string, string> {
                        { "valor", valor },
                        { "exception", e.Message },
                        { "stackTrace", e.StackTrace }
                    }
                );
                return BadRequest();
            }
        }

       
        [HttpPost()]
        [Authorize("Bearer")]
        [Route("updateOptinOptout")]
        public IActionResult UpdateOptin([FromBody]UpdateOptinOptout updateOptinOptout)
        {
            try
            {
                new ObaApiEcommerce.Control.Cliente.v3.ClienteControl().UpdateOptin(updateOptinOptout);
                Envelope.Message = "Dados atualizados com sucesso.";
                Envelope.Data = null;
                _telemetry.TrackEvent("[SUCCESS] POST /updateOptinOptout",
                    new Dictionary<string, string> {
                        { "cpf", $"{updateOptinOptout.cgccpf}-{updateOptinOptout.digcgccpf}" },
                        { "indenviomaladireta", updateOptinOptout.indenviomaladireta ? "S" : "N" },
                        { "indenviofax", updateOptinOptout.indenviofax ? "S" : "N" },
                        { "indcontatofone", updateOptinOptout.indcontatofone ? "S" : "N" },
                        { "indenvioemail", updateOptinOptout.indenvioemail ? "S" : "N" },
                    }
                );
                return Ok(Envelope);
            }
            catch(Exception e)
            {
                _telemetry.TrackEvent("[ERROR] POST /updateOptinOptout",
                    new Dictionary<string, string> {
                        { "cpf", $"{updateOptinOptout.cgccpf}-{updateOptinOptout.digcgccpf}" },
                        { "indenviomaladireta", updateOptinOptout.indenviomaladireta ? "S" : "N" },
                        { "indenviofax", updateOptinOptout.indenviofax ? "S" : "N" },
                        { "indcontatofone", updateOptinOptout.indcontatofone ? "S" : "N" },
                        { "indenvioemail", updateOptinOptout.indenvioemail ? "S" : "N" },
                        { "exception", e.Message },
                        { "stackTrace", e.StackTrace },
                    }
                );

                return BadRequest();
            }
        }
    }
}
