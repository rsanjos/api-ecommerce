﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Domain.Produto.v2
{
    public class Produto
    {

        /// <summary>
        /// Numero identificador da empresa.
        /// </summary>
        public int nroEmpresa { get; set; }

        /// <summary>
        /// Sequencial do produto.
        /// </summary>
        public long seqProduto { get; set; }

        /// <summary>
        /// Nome do produto para ecommerce.
        /// </summary>
        public string nomeProdutoEcomm { get; set; }

        /// <summary>
        /// Descrição detalhada do produto para ecommerce.
        /// </summary>
        public string descProdutoEcomm { get; set; }

        /// <summary>
        /// Nível categoria 1, código.
        /// </summary>
        public int codCateg_1 { get; set; }

        /// <summary>
        /// Nível categoria 1, descrição.
        /// </summary>
        public string Categoria_1 { get; set; }

        /// <summary>
        /// Nível categoria 2, código.
        /// </summary>
        public int codCateg_2 { get; set; }

        /// <summary>
        /// Nível categoria 2, descr~ição.
        /// </summary>
        public string categoria_2 { get; set; }

        /// <summary>
        /// Nível categoria 3, código.
        /// </summary>
        public int codCateg_3 { get; set; }

        /// <summary>
        /// Nível categoria 3, descrição.
        /// </summary>
        public string categoria_3 { get; set; }

        /// <summary>
        /// statusVenda  =  A - Ativo, I - Inativo.
        /// </summary>
        public string statusVenda { get; set; }

        /// <summary>
        /// indCcontrolaEstoqueEcom =  S- controla estque, N - não controla.
        /// </summary>
        public string indControlaEstoqueEcom { get; set; }

        /// <summary>
        /// indIntegraEcommerce = S- Produto habilitado para comercialização, N - Não habilitado.
        /// </summary>
        public string indIntegraEcommerce { get; set; }

        /// <summary>
        /// Url da imagem do produto.
        /// </summary>
        public string urlEcommerce { get; set; }


        /// <summary>
        /// Lista de Urls das imagens do produto.
        /// </summary>
        public List<string> urlEcommerceList { get; set; }

        internal  string auxurlEcommerceList { get; set; }

        /// <summary>
        /// Preço normal de venda.
        /// </summary>
        public double precoValidNormal { get; set; }

        /// <summary>
        /// Preço promocional de venda.
        /// </summary>
        public double precoValidPromoc { get; set; }

        /// <summary>
        /// Percentual de desconto sobre o preço normal.
        /// </summary>
        public string per_desconto { get; set; }

        /// <summary>
        /// Tipos: C - desconto para clientes em programas de fidelidade, P - produtos em promoção.
        /// </summary>
        public string tipoDesconto { get; set; }

        /// <summary>
        /// Data inicio promoção
        /// </summary>
        public string dtainicioPromocao { get; set; }

        /// <summary>
        /// Data fim promoção
        /// </summary>
        public string dtafimPromocao { get; set; }

        /// <summary>
        /// Unidade de venda do produto conseiderando a quntidade 1
        /// </summary>
        public string unidVenda { get; set; }
        public double estqLoja { get; set; }
       

        /// <summary>
        /// qtdMultiploVdaEcommerce  se Igual a Zero não possui multiplo de venda.
        /// </summary>
        public double qtdMultiploVdaEcommerce { get; set; }

        /// <summary>
        /// Indicador de bebida alcoolica.
        /// S - Contém alcool, N - não contém.
        /// </summary>
        public string bebidaalcoolica { get; set; }

        /// <summary>
        /// Lista dos códigos de acesso do produto.
        /// </summary>
        public List<CodAcesso> codigos { get; set; }
       


        public List<Produto> buscaProduto(DateTime dataBase, int nroEmpresa)
        {
            try
            {
                return new Dao.Produto.v2.ProdutoDao().buscaProduto(dataBase, nroEmpresa);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Produto buscaProduto(long seqProduto, int nroEmpresa)
        {
            try
            {
                return new Dao.Produto.v2.ProdutoDao().buscaProduto(seqProduto, nroEmpresa);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }

    

    public class CodAcesso
    {
        /// <summary>
        /// Sequrencial do produto.
        /// </summary>
        public long seqProduto { get; set; }

        /// <summary>
        /// código de acesso, string - tamanho 20.
        /// </summary>
        public string codAcesso { get; set; }

        /// <summary>
        /// Ativo para venda, S - Ativo, N - inativo.
        /// </summary>
        public string ativo { get; set; }

        /// <summary>
        ///Quantidade da embalagem cadastrada para o código.
        /// </summary>
        public double qtdEmbalagem { get; set; }

        /// <summary>
        /// Descrição da embalagem. 
        /// </summary>
        public string embalagem { get; set; }

        /// <summary>
        /// tipo = 'B' código Interno, 'E' - Ean, 'D' - Dun
        /// </summary>
        public string tipo { get; set; }
    }

    public class urlecommerceimg {     
      public string url { get; set; }
      public string indpricipal { get; set; }
    }
}
