﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Domain.Produto.v1
{
    public class Nutricionais
    {
        /// <summary>
        /// Sequrencial do produto.
        /// </summary>
        public long seqproduto { get; set; }

        /// <summary>
        /// Nome do produto para ecommerce.
        /// </summary>
        public string descricao { get; set; }

        /// <summary>
        /// Observações.
        /// </summary>
        public string observacao { get; set; }

        /// <summary>
        /// Quantidade da porção.
        /// </summary>
        public double qtdporcao { get; set; }

        /// <summary>
        /// Quantidade de valor energentico proporcionado pelo produto.
        /// </summary>
        public int valorenergetico { get; set; }

        /// <summary>
        /// Quantidade de carboidrato proporcionado pelo produto.
        /// </summary>
        public string carboidrato { get; set; }

        /// <summary>
        /// Quantidade de proteína proporcionado pelo produto.
        /// </summary>
        public string proteina { get; set; }

        /// <summary>
        /// Quantidade da substância encontradas no produto.
        /// </summary>
        public string gorduratotal { get; set; }

        /// <summary>
        /// Quantidade de gordura saturada proporcionado pelo produto.
        /// </summary>
        public int gordurasaturada { get; set; }

        /// <summary>
        /// Quantidade de gordura trans proporcionado pelo produto.
        /// </summary>
        public int gorduratrans { get; set; }

        /// <summary>
        /// Quantidade de fibra alimentar proporcionado pelo produto.
        /// </summary>
        public string fibraalimentar { get; set; }

        /// <summary>
        /// Quantidade de sódio proporcionado pelo produto.
        /// </summary>
        public string sodio { get; set; }
        /// <summary>
        /// Valor diário de referência de valor energetico.
        /// </summary>
        public double vlrDValorEnergetico { get; set; }

        /// <summary>
        /// Valor diário de referência de carboidrato. 
        /// </summary>
        public double vlrDCarboidrato { get; set; }

        /// <summary>
        /// Valor diário de referência gorduras totais. 
        /// </summary>
        public double vlrDProteina { get; set; }

        /// <summary>
        /// Valor diário de referência. 
        /// </summary>
        public double vlrDGorduraTotal { get; set; }

        /// <summary>
        /// Valor diário de referência de gorduras saturadas. 
        /// </summary>
        public double vlrDGorduraSaturada { get; set; }

        /// <summary>
        /// Valor diário de referência de gorduras trans.
        /// </summary>
        public double vlrDGorduraTrans { get; set; }
        /// <summary>
        /// Valor diário de referência de Fibras. 
        /// </summary>
        public double vlrDFibraAlimentar { get; set; }

        /// <summary>
        /// Valor diário de referência de sódio. 
        /// </summary>
        public double vlrDSodio { get; set; }


        internal object buscaNutricionais(long seqProduto)
        {
            try
            {
                return new Dao.Produto.v1.NutricionaisDao().buscaNutricionais(seqProduto);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
