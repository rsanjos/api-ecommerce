﻿using ObaApiEcommerce.Dao.Produto.v1;
using System;

namespace ObaApiEcommerce.Domain.Produto.v1
{
    internal class Promocao
    {
        /// <summary>
        /// Numero identificador da empresa .
        /// </summary>
        public long nroEmpresa { get; set; }

        /// <summary>
        /// Data inicio Promoção.
        /// </summary>
        public string dtaInicio { get; set; }

        /// <summary>
        /// Data fim Promoção.
        /// </summary>
        public string dtaFim { get; set; }

        /// <summary>
        /// Sequencial do produto.
        /// </summary>
        public long seqProduto { get; set; }

        /// <summary>
        /// Categoria nível 1.
        /// </summary>
        public string categoria_1 { get; set; }
        /// <summary>
        /// Categoria nível 2.
        /// </summary>
        public string categoria_2 { get; set; }
        /// <summary>
        /// Categoria nível 3.
        /// </summary>
        public string categoria_3 { get; set; }

        /// <summary>
        /// especificação detalhada do produto.
        /// </summary>
        public string especificdetalhada { get; set; }

        /// <summary>
        /// Descrição do produto para ecommerce.
        /// </summary>
        public string descecommerce { get; set; }

        /// <summary>
        /// Descrição do produto no erp.
        /// </summary>
        /// 
        public string produto { get; set; }

        /// <summary>
        /// Descrição da emblagem .
        /// </summary>
        /// 
        public string embalagem { get; set; }

        /// <summary>
        /// Preço normal de venda.
        /// </summary>
        public double precoValidNormal { get; set; }

        /// <summary>
        /// Preço promocional.
        /// </summary>
        public double precoPromocional { get; set; }

        /// <summary>
        /// Percentual de desconto sobre o preço normal.
        /// </summary>
        public string per_desconto { get; set; }
        /// <summary>
        /// Tipos: C - desconto para clientes em programas de fidelidade, P - produtos em promoção.
        /// </summary>
        public string tipoDesconto { get; set; }

        internal object buscaPromocao(long nroempresa)
        {
            return new ProdutoDao().buscaPromocao(nroempresa);
        }
    }
}