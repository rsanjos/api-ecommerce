﻿using System;

namespace ObaApiEcommerce.Domain.Produto.v1
{
    public class EstoqueProduto
    {
        /// <summary>
        /// Número identificador da empresa.
        /// </summary>
        public int nroempresa { get; set; }
        /// <summary>
        ///Sequencial do produto.
        /// </summary>
        public long seqProduto{ get; set; }

        /// <summary>
        /// Estoque disponível para venda.
        /// </summary>
        public double estqLoja{ get; set; }

        internal object buscaEstoque(long seqProduto, int nroEmpresa)
        {
            try
            {
                return new Dao.Produto.v1.ProdutoDao().BuscaEstqProduto(seqProduto, nroEmpresa);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}