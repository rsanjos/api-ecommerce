﻿using System;
using System.Collections.Generic;

namespace ObaApiEcommerce.Domain.Pedido.v1
{
    public class PedVendaFormaPagto
    {
        public long seqedipedvenda { get; set; }

        /// <summary>
        /// Sequencial gerado ao gravar a forma de pagamento.
        /// </summary>
        public long seqpedvendaformapagto { get; set; }

        /// <summary>
        /// Numero de identificação da forma de pagamento no erp
        /// </summary>
        public long nroformapagto { get; set; }

        //public long nrocondpagto { get; set; }

       /// <summary>
       /// Valor pago. Precisao de duas casas Decimais.
       /// </summary>
        public double  valor { get; set; }

        /// <summary>
        /// Código da operadora de cartão
        /// 1- 'Tecban'
        /// 4- 'Cielo'
        /// 5- 'Redecard'
        /// 6- 'Amex'
        /// 21- 'Banrisul'
        /// 31- 'Ticket combustível'
        /// 82- 'GetNet
        /// </summary>
        public long codoperadoracartao { get; set; }
        /// <summary>
        /// Número do Cartão presente.
        /// </summary>
        public long nrogiftcard { get; set; }

        /// <summary>
        /// Número do cartão de crédito/débito utilizado.
        /// </summary>
        public string nrocartao { get; set; }

        /// <summary>
        /// Quantidade de parcelas na referida forma de pagamento
        /// </summary>
        public int nroparcela { get; set; }

        /// <summary>
        /// Codigo da bandeira do cartão , 12 caracteres.
        /// </summary>
        public string codbandeira { get; set; }

        /// <summary>
        /// Número de Identificação Bancária.
        /// </summary>
        public string codBin { get; set; }

    }
}