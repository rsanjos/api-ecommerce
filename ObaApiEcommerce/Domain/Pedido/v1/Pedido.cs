﻿using Newtonsoft.Json;
using ObaApiEcommerce.Dao.Pedido.v1;
using ObaApiEcommerce.Utilitarios.Validacao;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Domain.Pedido.v1
{
    public class Pedido
    {

        /// <summary>
        /// Código sistema parceiro.
        /// </summary>
        [Required]
        public long codSistemaParceiro { get; set; }

        /// <summary>
        /// Número do pedido gerado no sistema parceiro.
        /// </summary>
        [Required]
        public long nroPedidoAfv { get; set; }

        /// <summary>
        /// Sequencial do pedido gerado no erp que será retornado para o parceiro.
        /// </summary>
        public long seqPedido { get; set; }

        /// <summary>
        /// L – Liberado para importação, R – Rejeitado(*), F – Finalizado(*). Será retornado para o sistema parceito
        /// </summary>
        public string statusPedido { get; set; }

        /// <summary>
        /// Squencial identificador do cliente no erp
        /// </summary>
        [Required]
        public long seqPessoa { get; set; }

        /// <summary>
        /// Sequencia que identifica o endereço do cliente.
        /// </summary>
        public long seqEndPessoa { get; set; }

       

        /// <summary>
        /// Numero do pedido gerado na expedição.  Será retornado para o sistema parceito.
        /// </summary>
        public long nroPedvenda { get; set;}

        /// <summary>
        /// Situação pedido gerado na expedição.  Será retornado para o sistema parceito.
        /// </summary>
        public string  statusPedvenda { get; set; }

        /// <summary>
        /// Numero da empresa do pedido.
        /// </summary>
        [Required]
        public long nroEmpresa  { get; set; }

        /// <summary>
        /// Forma de Pagamento principal do pedido.
        /// </summary>
        [Required]
        public long nroFormaPagto { get; set; }

        /// <summary>
        /// Data hora de Geração do pedido.
        /// </summary>
        /// 
        public DateTime dtaInclusao { get; set; }

        /// <summary>
        /// Observação do pedido, Tamamnho máximo 240 caracteres.
        /// </summary>
        public string ObservacaoPedido { get; set; }
        /// <summary>
        /// Indica se o pedido é para entrega ou se será retirado. E – Entrega, R – Retira.
        /// </summary>
        [Required]
        public string indEntregaRetira { get; set; }

        /// <summary>
        /// Data e hora do pedido no sistema parceiro, Formato dd/MM/yyyy hh24:mi:ss
        /// </summary>
        /// 
        [Required]
        public DateTime DtaPedidoAfv { get; set; }

        /// <summary>
        /// Valor do frete no caso de entrega
        /// </summary>
        [Required]
        public double vlrFrete { get; set; }

        /// <summary>
        /// Tipo de Rateio do frete.
        /// V - Rateio por Valor
        /// P - Rateio por Peso Bruto
        /// O - Rateio por Volume(m3)
        /// T - Conforme Cálculo de Frete do Transportador no Pedido de Venda.
        /// </summary>
        [Required]
        public string tiporateirofrete { get; set; }

        /// <summary>
        /// 6-Aguardando Pagamento, 7-Pagamento Confirmado.
        /// </summary>
        public string StatusPedEcomm { get; set; }

        /// <summary>
        /// Data e hora início da previsão de entrega,Formato: dd/MM/yyyy hh24:mi:ss
        /// </summary>
        /// 
        [Required]
        public DateTime  dtaHoraIniPrevEntrega { get; set; }

        /// <summary>
        /// Data e hora fim da previsão de entrega,Formato: dd/MM/yyyy hh24:mi:ss
        /// </summary>
        /// 
        [Required]
        public DateTime  dtaHoraFimPrevEntrega { get; set; }


        /// <summary>
        /// Data e hora de retirada do pedido, enviar maior que data e hora atual + o periodo mínimo de separação. Formato: dd/MM/yyyy hh24:mi:ss.
        /// </summary>
        public string dtaHorRetirada { get; set; }

        /// <summary>
        /// Lista dos itens Pedidos
        /// </summary>
        [Required]
        public  List<PedVendaItem> itensDoPedido { get; set; }

        /// <summary>
        /// Formas de pagamento do pedido
        /// </summary>
        //[Required]
        public List<PedVendaFormaPagto> formasDePagamento { get; set; }

        public List<PedVendaItemFaturado> itensFaturados { get; set; }


        /// <summary>
        /// Retorna a lista dos erros, caso houver.
        /// </summary>
        public  List<Erro> erros { get; set; }
        public object gravaPedido( Pedido ped)
        {
            try
            {
                ped.erros = new PedidoDao().validar(ped);
                if (ped.erros.Count == 0)
                {
                    ped = new PedidoDao().gravaPedido(ped);
                }
                else
                {
                    return ped;
                }
            }catch(Exception e)
            {
                throw e;
            }

            return ped;
        }

        internal static object buscaPedido(long codSistemaParceiro, long nroPedidoAfv)
        {
            try
            {
                Domain.Pedido.v1.Pedido ped = new PedidoDao().ConsultaPedido( codSistemaParceiro, nroPedidoAfv);
                return ped;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
