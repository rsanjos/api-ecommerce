﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Domain.Pedido.v1
{
    public class PedVendaItemFaturado
    {
        public DateTime dtaHoraCupom { get; set; }
        public string idCupom { get; set; }
        public long numerodf { get; set; }
        public long seqProduto { get; set; }
        public double qtdEmbalagem { get; set; }
        public double qtdAtendida { get; set; }
        public double qtdPedida { get; set; }
        public double vlrItem { get; set; }
    }

}

