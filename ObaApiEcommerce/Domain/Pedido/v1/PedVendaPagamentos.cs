﻿using System.ComponentModel.DataAnnotations;

namespace ObaApiEcommerce.Domain.Pedido.v1
{
    internal class PedVendaPagamentos
    {
        /// <summary>
        /// Numero identificador da forma de Pagamento no Erp
        /// </summary>
        public long nroformapagto { get; set; }

        /// <summary>
        /// Condição de pagamento do pedido
        /// </summary>
        public string nrocondpagto { get; set; }

        /// <summary>
        ///  Valor do pagamento
        /// </summary>
        public long valor { get; set; }

        /// <summary>
        /// Código da operadora de cartão
        ///1- 'Tecban'
        ///4- 'Cielo'
        ///5- 'Redecard'
        ///6- 'Amex'
        ///21- 'Banrisul'
        ///31- 'Ticket combustível'
        ///82- 'GetNet'
        ///* obrigatório preenchimento caso  seja venda na modalidade ‘cartão de crédito/débito’
        /// </summary>
      
        public double codoperadoracartao { get; set; }

       // public double nrogiftcard { get; set; }

        /// <summary>
        /// Numero do cartão.
        /// </summary>
        public double nrocartao { get; set; }

        /// <summary>
        /// Numero da parcela paga.
        /// </summary>
        public double nroparcela { get; set; }

        /// <summary>
        /// Codigo da bandeira do cartão , 12 caracteres.
        /// </summary>
        public string  codbandeira { get; set; }

        /// <summary>
        /// Número de Identificação Bancária.
        /// </summary>
        public string codBin { get; set; }

    }
}