﻿using System.ComponentModel.DataAnnotations;

namespace ObaApiEcommerce.Domain.Pedido.v1
{
    public class PedVendaItem
    {
        /// <summary>
        /// Sequencial gerado ao gravar o item, será retornado para o parceiro.
        /// </summary>
        public int seqpedVendaItem { get; set; }

        /// <summary>
        /// Codigo de acesso da embalagem pedida
        /// </summary>
        [Required]
        public string codacesso { get; set; }

        /// <summary>
        /// Sequencial identificador do produto no erp.
        /// </summary>
        [Required]
        public long seqproduto { get; set; }

        //public long nrocondicaopagto { get; set; }

        /// <summary>
        /// Quantidade da embalagem pedida.
        /// </summary>
        [Required]
        public double qtdpedida { get; set; }

        /// <summary>
        /// Quantidade da embalagem.
        /// </summary>
        [Required]
        public double qtdembalagem { get; set; }

        //public double vlrembtabpreco { get; set; }
        //public double vlrembtabpromoc { get; set; }

        /// <summary>
        /// Valor informado para a embalagem pedida
        /// </summary>
        [Required]
        public double vlrembinformado { get; set; }

        /// <summary>
        /// Valor de desconto se houver.
        /// </summary>
        public double vlrembdesconto { get; set; }
        //public long nrotabvenda { get; set; }

        /// <summary>
        /// Observação para o item do pedido. Tamanho máximo de 250 caracteres.
        /// </summary>
        /// 
        public string observacaoitem { get; set; }
        //public long nroempresaestq { get; set; }

        /// <summary>
        /// Indica se o produto permite similar.  S - Sim, N - Não
        /// </summary>
        [Required]
        public string indsimilarecommerce { get; set; }

    }
}