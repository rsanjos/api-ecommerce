﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Domain.Cliente.v3
{
    public class UpdateOptinOptout
    {
        [Required]
        public int cgccpf { get; set; }

        [Required]
        public int digcgccpf { get; set; }
        public bool indenviomaladireta { get; set; }
        public bool indcontatofone { get; set; }
        public bool indenviofax { get; set; }
        public bool indenvioemail { get; set; }
    }
}
