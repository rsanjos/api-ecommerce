﻿using ObaApiEcommerce.Dao.Cliente.v1;
using ObaApiEcommerce.Utilitarios.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Domain.Cliente.v1
{
    public class Endereco
    {
        [Required]
        public long seqPessoa { get; set; }

        /// <summary>
        ///  Se o seqEndereco for mair que Zero indica uma atualização, se não, indica um novo cadastro.
        ///  o valor deste atributo nunca deve ser menor que Zero.
        /// </summary>
        [Required]
        [DefaultValue(0)]
        public int seqEndereco { get; set; }


        /// <summary>
        /// tipoEndereco = C - Cobrança / E - Entrega 
        /// </summary>
        [Required]
        public string tipoEndereco { get; set; }

        [Required]
        public string bairro { get; set; }

        [Required]
        public string logradouro { get; set; }

        [Required]
        public string cep { get; set; }

        [Required]
        public string cidade { get; set; }

        [Required]
        public string uf { get; set; }

        public string cmpltoLogradouro { get; set; }
        public string nroLogradouro { get; set; }
        public string descricaoEnd { get; set; }
        public string foneEnd { get; set; }
        public string nomeRecebedor { get; set; }
        public string emailRecebedor { get; set; }


        internal List<Endereco> Cadastrar(Endereco endereco)
        {

            return new EnderecoDao().cadastrar(endereco);
        }


        internal List<Endereco> buscar(long seqPessoa)
        {

            return new EnderecoDao().buscar(seqPessoa);
        }

    }

}
