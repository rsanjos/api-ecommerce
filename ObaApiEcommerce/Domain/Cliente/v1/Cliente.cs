﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Domain.Cliente.v1
{
    public class Cliente
    {
        public int seqPessoa { get; set; }
        [Required]
        public string nomeRazao { get; set; }
        public string sexo { get; set; }
        public string fantasia { get; set; }

        [Required]
        public string cgccpf { get; set; }

        [Required]
        public string digcgccpf { get; set; }
        public string foneddd1 { get; set; }
        public string fonenro1 { get; set; }

        [Required]
        public string fisicaJuridica { get; set; }

        [Required]
        public string cidade { get; set; }

        [Required]
        public string uf { get; set; }

        [Required]
        public string pais { get; set; }


        public string bairro { get; set; }


        [Required]
        public string logradouro { get; set; }


        public string nroLogradouro { get; set; }

        public string cmpltoLogradouro { get; set; }


        [Required]
        public string cep { get; set; }
        public string telefone { get; set; }
        public string inscricaorg { get; set; }
        public string inscmunicipal { get; set; }

        [Required]
        public string email { get; set; }

        [Required]
        public DateTime dtanascfund { get; set; }

        /// <summary>
        /// Aceita receber mala direta?
        /// </summary>
        public string indenviomaladireta { get; set; }

        /// <summary>
        /// Aceita receber contato por telefone?
        /// </summary>
        public string indcontatofone { get; set; }

        /// <summary>
        /// Aceita receber contato por fax?
        /// </summary>
        public string indenviofax { get; set; }

        /// <summary>
        /// Aceita receber contato por e-mail?
        /// </summary>
        public string indenvioemail { get; set; }


        public string  dtaCadastro { get; set; }

        /// <summary>
        /// statusCliente: A - Ativo, I - Inativo
        /// </summary>
        public string statusCliente { get; set; }
        public string dtaUltcompra { get; set; }

        /// <summary>
        /// Origem do cadastro, limite de 12  caracteres.
        /// </summary>
        public string origem { get; set; }

        public List<Endereco> enderecos { get; set; }

        
        public Cliente buscar(string  email)

        {
            try
            {
                Cliente cliente = new   Dao.Cliente.v1.ClienteDao().buscaCliente(email);
                
                return cliente;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Cliente cadastrar(Cliente cliente)

        {
            try
            {
                cliente  =  new Dao.Cliente.v1.ClienteDao().CadastrarCliente(cliente);
                return cliente;
            }
            catch(Exception e)
            {
                throw e;
            }
        }




    }

    
}
