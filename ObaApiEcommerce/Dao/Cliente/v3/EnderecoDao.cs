﻿using ObaApiEcommerce.Domain.Cliente.v3;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Cliente.v3
{
    public class EnderecoDao
    {

        internal List<Endereco> buscar(long seqPessoa)
        {
            using Connection conexao = new Connection(Connection.CON_SP);
            OracleCommand comand = new OracleCommand();
            comand.Parameters.Add(new OracleParameter("pr_seqcliente", seqPessoa));

            OracleParameter re_result = new OracleParameter();
            re_result.ParameterName = "r_result";
            re_result.OracleDbType = OracleDbType.RefCursor;
            re_result.Direction = ParameterDirection.Output;
            comand.Parameters.Add(re_result);
            comand.CommandText = "pkg_cliente.p_buscaEndereco";
            conexao.getResultfromCommand(comand);
            OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

            return new ReadSQLToObject().DataReaderList(reader, new Endereco().GetType()).OfType<Endereco>().ToList();
        }

        internal List<Endereco> cadastrar(Endereco endereco)
        {
            using Connection conexao = new Connection(Connection.CON_SP);
            OracleCommand comand = new OracleCommand();
            comand.Parameters.Add(new OracleParameter("p_seqpessoa", endereco.seqPessoa));
            comand.Parameters.Add(new OracleParameter("p_seqpessoaEnd", endereco.seqEndereco));
            comand.Parameters.Add(new OracleParameter("p_tipoendereco", endereco.tipoEndereco));
            comand.Parameters.Add(new OracleParameter("p_bairro", endereco.bairro));
            comand.Parameters.Add(new OracleParameter("p_logradouro", endereco.logradouro));
            comand.Parameters.Add(new OracleParameter("p_cep", endereco.cep));
            comand.Parameters.Add(new OracleParameter("p_cidade", endereco.cidade));
            comand.Parameters.Add(new OracleParameter("p_uf", endereco.uf));
            comand.Parameters.Add(new OracleParameter("p_cmpltologradouro", !string.IsNullOrEmpty(endereco.cmpltoLogradouro) ? (object)endereco.cmpltoLogradouro : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("p_nrologradouro", !string.IsNullOrEmpty(endereco.nroLogradouro) ? (object)endereco.nroLogradouro : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("p_descricaoend", !string.IsNullOrEmpty(endereco.descricaoEnd) ? (object)endereco.descricaoEnd : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("p_foneend", !string.IsNullOrEmpty(endereco.foneEnd) ? (object)endereco.foneEnd : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("p_nomerecebedor", !string.IsNullOrEmpty(endereco.nomeRecebedor) ? (object)endereco.nomeRecebedor : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("p_emailrecebedor", !string.IsNullOrEmpty(endereco.emailRecebedor) ? (object)endereco.emailRecebedor : DBNull.Value));

            OracleParameter re_result = new OracleParameter();
            re_result.ParameterName = "r_result";
            re_result.OracleDbType = OracleDbType.RefCursor;
            re_result.Direction = ParameterDirection.Output;
            comand.Parameters.Add(re_result);
            comand.CommandText = "pkg_cliente.p_cadastrarEndereco";
            conexao.getResultfromCommand(comand);
            OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

            return new ReadSQLToObject().DataReaderList(reader, new Endereco().GetType()).OfType<Endereco>().ToList();
        }


        internal List<Endereco> buscar(long seqPessoa, Connection conexao)
        {
            OracleCommand comand = new OracleCommand();
            comand.Parameters.Add(new OracleParameter("pr_seqcliente", seqPessoa));

            OracleParameter re_result = new OracleParameter();
            re_result.ParameterName = "r_result";
            re_result.OracleDbType = OracleDbType.RefCursor;
            re_result.Direction = ParameterDirection.Output;
            comand.Parameters.Add(re_result);
            comand.CommandText = "pkg_cliente.p_buscaEndereco";
            conexao.getResultfromCommand(comand);
            OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

            return new ReadSQLToObject().DataReaderList(reader, new Endereco().GetType()).OfType<Endereco>().ToList();
        }
    }
}
