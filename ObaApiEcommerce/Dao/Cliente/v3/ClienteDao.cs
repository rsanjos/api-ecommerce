﻿using ObaApiEcommerce.Dao.Cliente.v3;
using ObaApiEcommerce.Domain.Cliente.v3;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Cliente.v3
{
    public class ClienteDao
    {
        internal ObaApiEcommerce.Domain.Cliente.v3.Cliente CadastrarCliente(ObaApiEcommerce.Domain.Cliente.v3.Cliente cliente)
        {
            using Connection conexao = new Connection(Connection.CON_SP);
            OracleCommand comand = new OracleCommand();
            comand.Parameters.Add(new OracleParameter("pr_nomerazao", cliente.nomeRazao));
            comand.Parameters.Add(new OracleParameter("pr_fantasia", !string.IsNullOrEmpty(cliente.fantasia) ? (object)cliente.fantasia : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_sexo", !string.IsNullOrEmpty(cliente.sexo) ? (object)cliente.sexo : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_nrocgccpf", cliente.cgccpf));
            comand.Parameters.Add(new OracleParameter("pr_digcgccpf", cliente.digcgccpf));
            comand.Parameters.Add(new OracleParameter("pr_fisicajuridica", cliente.fisicaJuridica));
            comand.Parameters.Add(new OracleParameter("pr_logradouro", !string.IsNullOrEmpty(cliente.logradouro) ? (object)cliente.logradouro : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_nrologradouro", !string.IsNullOrEmpty(cliente.nroLogradouro) ? (object)cliente.nroLogradouro : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_cmpltologradouro", !string.IsNullOrEmpty(cliente.cmpltoLogradouro) ? (object)cliente.cmpltoLogradouro : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_bairro", !string.IsNullOrEmpty(cliente.bairro) ? (object)cliente.bairro : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_cep", !string.IsNullOrEmpty(cliente.cep) ? (object)cliente.cep : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_cidade", !string.IsNullOrEmpty(cliente.cidade) ? (object)cliente.cidade : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_uf", !string.IsNullOrEmpty(cliente.uf) ? (object)cliente.uf : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_email", !string.IsNullOrEmpty(cliente.email) ? (object)cliente.email : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_foneddd1", !string.IsNullOrEmpty(cliente.foneddd1) ? (object)cliente.foneddd1 : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_fonenro1", !string.IsNullOrEmpty(cliente.fonenro1) ? (object)cliente.fonenro1 : DBNull.Value));
            comand.Parameters.Add(new OracleParameter("pr_dtanascfund", (object)cliente.dtanascfund));
            comand.Parameters.Add(new OracleParameter("pr_indenvioemail", !string.IsNullOrEmpty(cliente.indenvioemail) ? (object)cliente.indenvioemail : "N"));
            comand.Parameters.Add(new OracleParameter("pr_indcontatofone", !string.IsNullOrEmpty(cliente.indcontatofone) ? (object)cliente.indcontatofone : "N"));
            comand.Parameters.Add(new OracleParameter("pr_indenviomaladireta", !string.IsNullOrEmpty(cliente.indenviomaladireta) ? (object)cliente.indenviomaladireta : "N"));
            comand.Parameters.Add(new OracleParameter("pr_indenviofax", !string.IsNullOrEmpty(cliente.indenviofax) ? (object)cliente.indenviofax : "N"));
            comand.Parameters.Add(new OracleParameter("pr_origem", !string.IsNullOrEmpty(cliente.origem) ? (object)cliente.origem : "API-OBA-ECOM"));

            OracleParameter re_result = new OracleParameter
            {
                ParameterName = "r_result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.Output
            };
            comand.Parameters.Add(re_result);
            comand.CommandText = "pkg_cliente.P_CADASTRACLIENTE_V3";
            conexao.getResultfromCommand(comand);
            OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

            cliente = buscaCliente(!string.IsNullOrEmpty(cliente.email) ? cliente.email : cliente.cgccpf + cliente.digcgccpf);

            if (cliente.seqPessoa > 0)
            {
                cliente.enderecos = new EnderecoDao().buscar(cliente.seqPessoa, conexao);
            }

            return cliente;
        }

        internal Domain.Cliente.v3.Cliente buscaCliente(string email)
        {
            Domain.Cliente.v3.Cliente cliente = new Domain.Cliente.v3.Cliente();
            using Connection conexao = new Connection(Connection.CON_SP);
            OracleCommand comand = new OracleCommand();
            comand.Parameters.Add(new OracleParameter("ps_email", email));
            OracleParameter re_result = new OracleParameter
            {
                ParameterName = "r_result",
                OracleDbType = OracleDbType.RefCursor,
                Direction = ParameterDirection.Output
            };
            comand.Parameters.Add(re_result);
            comand.CommandText = "pkg_cliente.p_buscaCliente";
            conexao.getResultfromCommand(comand);
            OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

            cliente = (ObaApiEcommerce.Domain.Cliente.v3.Cliente)new ReadSQLToObject().DataReaderObject(reader, cliente);

            if (cliente.seqPessoa > 0)
            {
                cliente.enderecos = new EnderecoDao().buscar(cliente.seqPessoa, conexao);
            }

            return (cliente.seqPessoa > 0) ? cliente : null;
        }


        public void UpdateOptin(UpdateOptinOptout updateOptinOptout)
        {
            using Connection conexao = new Connection(Connection.CON_SP);
            OracleCommand comand = new OracleCommand();
            
            comand.Parameters.Add(new OracleParameter("pr_nrocgccpf", updateOptinOptout.cgccpf));
            comand.Parameters.Add(new OracleParameter("pr_digcgccpf", updateOptinOptout.digcgccpf));
            comand.Parameters.Add(new OracleParameter("pr_indenvioemail", updateOptinOptout.indenvioemail ? "S" : "N"));
            comand.Parameters.Add(new OracleParameter("pr_indcontatofone", updateOptinOptout.indcontatofone ? "S" : "N"));
            comand.Parameters.Add(new OracleParameter("pr_indenviomaladireta", updateOptinOptout.indenviomaladireta ? "S" : "N"));
            comand.Parameters.Add(new OracleParameter("pr_indenviofax", updateOptinOptout.indenviofax ? "S" : "N"));

            comand.CommandText = "pkg_cliente.P_ATUALIZAOPTINOPTOUT_V3";

            conexao.getResultfromCommand(comand);
        }
    }
}
