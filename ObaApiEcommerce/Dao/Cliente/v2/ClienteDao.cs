﻿using ObaApiEcommerce.Dao.Cliente.v2;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Cliente.v2
{
    public class ClienteDao
    {
        internal ObaApiEcommerce.Domain.Cliente.v1.Cliente CadastrarCliente(ObaApiEcommerce.Domain.Cliente.v1.Cliente cliente)
        {
            try
            {
                using (Connection conexao = new Connection(Connection.CON_SP))
                {
                    OracleCommand comand = new OracleCommand();
                    comand.Parameters.Add(new OracleParameter("pr_nomerazao", cliente.nomeRazao));
                    comand.Parameters.Add(new OracleParameter("pr_fantasia", !string.IsNullOrEmpty(cliente.fantasia) ? (object)cliente.fantasia : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_sexo", !string.IsNullOrEmpty(cliente.sexo) ? (object)cliente.sexo : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_nrocgccpf", cliente.cgccpf));
                    comand.Parameters.Add(new OracleParameter("pr_digcgccpf", cliente.digcgccpf));
                    comand.Parameters.Add(new OracleParameter("pr_fisicajuridica", cliente.fisicaJuridica));
                    comand.Parameters.Add(new OracleParameter("pr_logradouro", !string.IsNullOrEmpty(cliente.logradouro) ? (object)cliente.logradouro : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_nrologradouro", !string.IsNullOrEmpty(cliente.nroLogradouro) ? (object)cliente.nroLogradouro : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_cmpltologradouro", !string.IsNullOrEmpty(cliente.cmpltoLogradouro) ? (object)cliente.cmpltoLogradouro : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_bairro", !string.IsNullOrEmpty(cliente.bairro) ? (object)cliente.bairro : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_cep", !string.IsNullOrEmpty(cliente.cep) ? (object)cliente.cep : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_cidade", !string.IsNullOrEmpty(cliente.cidade) ? (object)cliente.cidade : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_uf", !string.IsNullOrEmpty(cliente.uf) ? (object)cliente.uf : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_email", !string.IsNullOrEmpty(cliente.email) ? (object)cliente.email : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_foneddd1", !string.IsNullOrEmpty(cliente.foneddd1) ? (object)cliente.foneddd1 : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_fonenro1", !string.IsNullOrEmpty(cliente.fonenro1) ? (object)cliente.fonenro1 : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_dtanascfund", (object)cliente.dtanascfund));
                    comand.Parameters.Add(new OracleParameter("pr_indenvioemail", !string.IsNullOrEmpty(cliente.indenviomaladireta) ? (object)cliente.indenviomaladireta : "N"));
                    comand.Parameters.Add(new OracleParameter("pr_indcontatofone", !string.IsNullOrEmpty(cliente.indcontatofone) ? (object)cliente.indcontatofone : "N"));
                    comand.Parameters.Add(new OracleParameter("pr_origem", !string.IsNullOrEmpty(cliente.origem) ? (object)cliente.origem : "API-OBA-ECOM"));

                    OracleParameter re_result = new OracleParameter();
                    re_result.ParameterName = "r_result";
                    re_result.OracleDbType = OracleDbType.RefCursor;
                    re_result.Direction = ParameterDirection.Output;
                    comand.Parameters.Add(re_result);
                    comand.CommandText = "pkg_cliente.p_cadastraClientev2";
                    conexao.getResultfromCommand(comand);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

                    //ObaApiEcommerce.Domain.Cliente.v1.Cliente x  = (ObaApiEcommerce.Domain.Cliente.v1.Cliente)new ReadSQLToObject().DataReaderObject(reader, new ObaApiEcommerce.Domain.Cliente.v1.Cliente().GetType());

                    cliente = buscaCliente(!string.IsNullOrEmpty(cliente.email) ? cliente.email : cliente.cgccpf + cliente.digcgccpf);

                    if (cliente.seqPessoa > 0)
                    {
                       // cliente.enderecos = new EnderecoDao().buscar(cliente.seqPessoa, conexao);
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }

            return cliente;
        }

        internal Domain.Cliente.v1.Cliente buscaCliente(string email)
        {
            Domain.Cliente.v1.Cliente cliente = new Domain.Cliente.v1.Cliente();
            try
            {
                using (Connection conexao = new Connection(Connection.CON_SP))
                {
                    OracleCommand comand = new OracleCommand();
                    comand.Parameters.Add(new OracleParameter("ps_email", email));
                    OracleParameter re_result = new OracleParameter();
                    re_result.ParameterName = "r_result";
                    re_result.OracleDbType = OracleDbType.RefCursor;
                    re_result.Direction = ParameterDirection.Output;
                    comand.Parameters.Add(re_result);
                    comand.CommandText = "pkg_cliente.p_buscaCliente";
                    conexao.getResultfromCommand(comand);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

                    cliente = (ObaApiEcommerce.Domain.Cliente.v1.Cliente)new ReadSQLToObject().DataReaderObject(reader, cliente);

                    if (cliente.seqPessoa > 0)
                    {
                        cliente.enderecos = new EnderecoDao().buscar(cliente.seqPessoa, conexao);
                    }
                }

                if (cliente.seqPessoa > 0)
                {
                    return cliente;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public void inativarCliente (string pr_nrocgccpf, string pr_digcgccpf)
        {
            try
            {
                using (Connection conexao = new Connection(Connection.CON_SP))
                {
                    OracleCommand comand = new OracleCommand();
                    comand.Parameters.Add(new OracleParameter("pr_nrocgccpf", pr_nrocgccpf));
                    comand.Parameters.Add(new OracleParameter("pr_digcgccpf", pr_digcgccpf));

                    OracleParameter re_result = new OracleParameter();
                    re_result.ParameterName = "r_result";
                    re_result.OracleDbType = OracleDbType.RefCursor;
                    re_result.Direction = ParameterDirection.Output;
                    // comand.Parameters.Add(re_result);
                    comand.CommandText = "pkg_cliente.P_INATIVACLIENTE";

                    conexao.getResultfromCommand(comand);
                    
                }
            }catch(Exception e)
            {
                throw new Exception ( "Não foi possível inativar o cliente.");
            }

        }
    }
}
