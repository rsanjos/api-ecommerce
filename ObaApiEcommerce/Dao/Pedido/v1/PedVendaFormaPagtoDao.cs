﻿using ObaApiEcommerce.Domain.Pedido.v1;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using ObaApiEcommerce.Utilitarios.Validacao;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Pedido.v1
{
    public class PedVendaFormaPagtoDao
    {
        internal List<PedVendaFormaPagto> gravaPagamentos(List<PedVendaFormaPagto> formasDePagamento, Domain.Pedido.v1.Pedido ped, Connection conexao)
        {
            List<PedVendaFormaPagto> auxPagto = new List<PedVendaFormaPagto>();
            foreach (var item in formasDePagamento)
            {

                try
                {
                        OracleCommand comand = new OracleCommand();
                        comand.Parameters.Add(new OracleParameter("pr_seqedipedvenda", ped.seqPedido));
                        comand.Parameters.Add(new OracleParameter("pr_nroformapagto", item.nroformapagto));
                        comand.Parameters.Add(new OracleParameter("pr_nrocondpagto", 999));
                        comand.Parameters.Add(new OracleParameter("pr_valor", item.valor));
                        comand.Parameters.Add(new OracleParameter("pr_codoperadoracartao", item.codoperadoracartao));
                        comand.Parameters.Add(new OracleParameter("pr_nrogiftcard", null));
                        comand.Parameters.Add(new OracleParameter("pr_nrocartao", item.nrocartao));
                        comand.Parameters.Add(new OracleParameter("pr_nroparcela", item.nroparcela));
                        comand.Parameters.Add(new OracleParameter("pr_codbandeira", !string.IsNullOrEmpty(item.codbandeira) ? (object)item.codbandeira : DBNull.Value));
                        comand.Parameters.Add(new OracleParameter("pr_codbin", !string.IsNullOrEmpty(item.codBin) ? (object)item.codBin : DBNull.Value));
                        

                        OracleParameter re_result = new OracleParameter();
                        re_result.ParameterName = "v_seqpedvendaformapagto";
                        re_result.OracleDbType = OracleDbType.Int64;
                        re_result.Direction = ParameterDirection.Output;
                        comand.Parameters.Add(re_result);
                        comand.CommandText = "pkg_pedidocliente.p_pedVendaFormaPagto";
                        conexao.getResultfromCommand(comand);
                        item.seqpedvendaformapagto = long.Parse(((Oracle.ManagedDataAccess.Types.OracleDecimal)comand.Parameters["v_seqpedvendaformapagto"].Value).Value.ToString());

                        auxPagto.Add(item);

                }
                catch (Exception e)
                {
                    throw e;
                }

            }

            return auxPagto;
        }

        internal List<Erro> validarFormaPgto(Domain.Pedido.v1.Pedido ped, Connection conexao)
        {
            List<Erro> erros = new List<Erro>();
            foreach (var item in ped.formasDePagamento)
            {

                try
                {
                    OracleCommand comand = new OracleCommand();
                    comand.Parameters.Add(new OracleParameter("pr_seqedipedvenda", ped.seqPedido));
                    comand.Parameters.Add(new OracleParameter("pr_nroformapagto", item.nroformapagto));
                    comand.Parameters.Add(new OracleParameter("pr_nrocondpagto", 999));
                    comand.Parameters.Add(new OracleParameter("pr_valor", item.valor));
                    comand.Parameters.Add(new OracleParameter("pr_codoperadoracartao", item.codoperadoracartao));
                    comand.Parameters.Add(new OracleParameter("pr_nrogiftcard", null));
                    comand.Parameters.Add(new OracleParameter("pr_nrocartao", item.nrocartao));
                    comand.Parameters.Add(new OracleParameter("pr_nroparcela", item.nroparcela));
                    comand.Parameters.Add(new OracleParameter("pr_codbandeira", !string.IsNullOrEmpty(item.codbandeira) ? (object)item.codbandeira : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_codbin", !string.IsNullOrEmpty(item.codBin) ? (object)item.codBin : DBNull.Value));


                    OracleParameter re_result = new OracleParameter();
                    re_result.ParameterName = "r_result";
                    re_result.OracleDbType = OracleDbType.RefCursor;
                    re_result.Direction = ParameterDirection.Output;
                    comand.Parameters.Add(re_result);
                    comand.CommandText = "pkg_pedidocliente.p_validaFormaPagto";
                    conexao.getResultfromCommand(comand);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

                    erros.AddRange( new ReadSQLToObject().DataReaderList(reader, new Erro().GetType()).OfType<Erro>().ToList());

                }
                catch (Exception e)
                {
                    throw e;
                }

            }

            return erros;
        }
    }
}
