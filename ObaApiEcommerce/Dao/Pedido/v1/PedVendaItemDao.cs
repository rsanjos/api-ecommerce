﻿using Microsoft.Extensions.Caching.Memory;
using ObaApiEcommerce.Domain.Pedido.v1;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using ObaApiEcommerce.Utilitarios.Validacao;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Pedido.v1
{
    public class PedVendaItemDao
    {
        internal List<PedVendaItem> gravarItens(List<PedVendaItem> itensDoPedido, Domain.Pedido.v1.Pedido ped, Connection conexao)
        {

            List<PedVendaItem> auxItens = new List<PedVendaItem>();

            try
            {
                    foreach (var item in itensDoPedido)
                    {

                        OracleCommand comand = new OracleCommand();
                        comand.Parameters.Add(new OracleParameter("pr_seqedipedvenda", ped.seqPedido));
                        comand.Parameters.Add(new OracleParameter("pr_codacesso", item.codacesso));
                        comand.Parameters.Add(new OracleParameter("pr_seqproduto", item.seqproduto));
                        comand.Parameters.Add(new OracleParameter("pr_nrocondicaopagto", DBNull.Value ));
                        comand.Parameters.Add(new OracleParameter("pr_qtdpedida", item.qtdpedida));
                        comand.Parameters.Add(new OracleParameter("pr_qtdembalagem", item.qtdembalagem));
                        comand.Parameters.Add(new OracleParameter("pr_vlrembtabpreco", DBNull.Value));
                        comand.Parameters.Add(new OracleParameter("pr_vlrembtabpromoc", DBNull.Value));
                        comand.Parameters.Add(new OracleParameter("pr_vlrembinformado", item.vlrembinformado));
                        comand.Parameters.Add(new OracleParameter("pr_vlrembdesconto", item.vlrembdesconto));
                        //comand.Parameters.Add(new OracleParameter("pr_nrotabvenda", 1));
                        comand.Parameters.Add(new OracleParameter("pr_observacaoitem", !string.IsNullOrEmpty(item.observacaoitem) ? (object)item.observacaoitem : DBNull.Value));
                        comand.Parameters.Add(new OracleParameter("pr_nroempresaestq", ped.nroEmpresa));
                        comand.Parameters.Add(new OracleParameter("pr_indsimilarecommerce", item.indsimilarecommerce));

                        OracleParameter re_result = new OracleParameter();
                        re_result.ParameterName = "v_seqpedvendaitem";
                        re_result.OracleDbType = OracleDbType.Int64;
                        re_result.Direction = ParameterDirection.Output;
                        comand.Parameters.Add(re_result);
                        comand.CommandText = "pkg_pedidocliente.p_pedvendaitem";
                        conexao.getResultfromCommand(comand);

                        //var t = (comand.Parameters["v_seqpedvendaitem"].Value.ToString());
                        item.seqpedVendaItem = int.Parse((comand.Parameters["v_seqpedvendaitem"].Value.ToString()));

                        auxItens.Add(item);
                    }


            }
            catch (Exception e)
            {
                throw e;
            }

            return auxItens;
        }

        internal List<Erro> validarItens(Domain.Pedido.v1.Pedido ped, Connection conexao)
        {

            List<Erro> erros = new List<Erro>();
            try
            {
                foreach (var item in ped.itensDoPedido)
                {

                    OracleCommand comand = new OracleCommand();
                    comand.Parameters.Add(new OracleParameter("pr_seqedipedvenda", ped.seqPedido));
                    comand.Parameters.Add(new OracleParameter("pr_codacesso", item.codacesso));
                    comand.Parameters.Add(new OracleParameter("pr_seqproduto", item.seqproduto));
                    comand.Parameters.Add(new OracleParameter("pr_nrocondicaopagto", DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_qtdpedida", item.qtdpedida));
                    comand.Parameters.Add(new OracleParameter("pr_qtdembalagem", item.qtdembalagem));
                    comand.Parameters.Add(new OracleParameter("pr_vlrembtabpreco", DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_vlrembtabpromoc", DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_vlrembinformado", item.vlrembinformado));
                    comand.Parameters.Add(new OracleParameter("pr_vlrembdesconto", item.vlrembdesconto));
                    //comand.Parameters.Add(new OracleParameter("pr_nrotabvenda", 1));
                    comand.Parameters.Add(new OracleParameter("pr_observacaoitem", !string.IsNullOrEmpty(item.observacaoitem) ? (object)item.observacaoitem : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_nroempresa", ped.nroEmpresa));
                    comand.Parameters.Add(new OracleParameter("pr_indsimilarecommerce", item.indsimilarecommerce));

                    OracleParameter re_result = new OracleParameter();
                    re_result.ParameterName = "r_result";
                    re_result.OracleDbType = OracleDbType.RefCursor;
                    re_result.Direction = ParameterDirection.Output;
                    comand.Parameters.Add(re_result);
                    comand.CommandText = "pkg_pedidocliente.p_validaItensPedido";
                    conexao.getResultfromCommand(comand);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

                    erros.AddRange( new ReadSQLToObject().DataReaderList(reader, new Erro().GetType()).OfType<Erro>().ToList());
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return erros;
        }

        internal List<PedVendaItemFaturado> buscaItensFaturados(long nroPedvenda, Connection conexao)
        {
            try
            {
                OracleCommand comand = new OracleCommand();
                comand.Parameters.Add(new OracleParameter("pr_nroPedvenda", nroPedvenda));

                OracleParameter re_result = new OracleParameter();
                re_result.ParameterName = "r_result";
                re_result.OracleDbType = OracleDbType.RefCursor;
                re_result.Direction = ParameterDirection.Output;
                comand.Parameters.Add(re_result);
                comand.CommandText = "pkg_pedidocliente.p_buscaItensPedido";
                conexao.getResultfromCommand(comand);
                OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

                return new ReadSQLToObject().DataReaderList(reader, new ObaApiEcommerce.Domain.Pedido.v1.PedVendaItemFaturado().GetType()).OfType<ObaApiEcommerce.Domain.Pedido.v1.PedVendaItemFaturado>().ToList();

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
