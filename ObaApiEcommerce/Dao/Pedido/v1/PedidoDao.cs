﻿using ObaApiEcommerce.Domain.Pedido.v1;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using ObaApiEcommerce.Utilitarios.Validacao;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Pedido.v1
{
    public class PedidoDao
    {
        internal Domain.Pedido.v1.Pedido gravaPedido(Domain.Pedido.v1.Pedido ped)
        {
            try
            {
                using (Connection conexao = new Connection(Connection.CON_SP))
                {
                    OracleCommand comand = new OracleCommand();

                    DateTime aux_pr_dtahorretirada = !string.IsNullOrEmpty(ped.dtaHorRetirada) ? DateTime.Parse(ped.dtaHorRetirada) : DateTime.Now;

                    comand.Parameters.Add(new OracleParameter("pr_codsistemaafv", ped.codSistemaParceiro));
                    comand.Parameters.Add(new OracleParameter("pr_nropedidoafv", ped.nroPedidoAfv));
                    comand.Parameters.Add(new OracleParameter("pr_nroempresa", ped.nroEmpresa));
                    comand.Parameters.Add(new OracleParameter("pr_nroformapagto", ped.nroFormaPagto));
                    comand.Parameters.Add(new OracleParameter("pr_indentregaretira", ped.indEntregaRetira));
                    comand.Parameters.Add(new OracleParameter("pr_datahorapedido", ped.DtaPedidoAfv));
                    comand.Parameters.Add(new OracleParameter("pr_vlrfrete", ped.vlrFrete));
                    comand.Parameters.Add(new OracleParameter("pr_tiporateirofrete", ped.tiporateirofrete));
                    comand.Parameters.Add(new OracleParameter("pr_dtaprevinientrega", ped.dtaHoraIniPrevEntrega));
                    comand.Parameters.Add(new OracleParameter("pr_dtaprevfimentrega", ped.dtaHoraFimPrevEntrega));
                    comand.Parameters.Add(new OracleParameter("pr_seqpessoa", ped.seqPessoa));
                    comand.Parameters.Add(new OracleParameter("pr_seqpessoaend",  ped.seqEndPessoa > 0 ? (object)ped.seqEndPessoa : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_dtahorretirada",!string.IsNullOrEmpty(ped.dtaHorRetirada) ? (object)aux_pr_dtahorretirada : DBNull.Value));
                    comand.Parameters.Add(new OracleParameter("pr_obspedido", ped.ObservacaoPedido));

                    OracleParameter re_result = new OracleParameter();
                    re_result.ParameterName = "v_seqedipedvenda";
                    re_result.OracleDbType = OracleDbType.Int64;
                    re_result.Direction = ParameterDirection.Output;
                    comand.Parameters.Add(re_result);
                    comand.CommandText = "pkg_pedidocliente.p_pedidafv";
                    conexao.getResultfromCommand(comand);
                    ped.seqPedido = long.Parse( ((Oracle.ManagedDataAccess.Types.OracleDecimal)comand.Parameters["v_seqedipedvenda"].Value).Value.ToString());

                    if (ped.seqPedido > 0)
                    {
                        ped.itensDoPedido = new PedVendaItemDao().gravarItens(ped.itensDoPedido, ped, conexao);
                        ped.formasDePagamento = new PedVendaFormaPagtoDao().gravaPagamentos(ped.formasDePagamento, ped, conexao);
                        //new PedidoDao().updateStatus(ped.seqPedido, "L");
                        ped.nroPedvenda = new PedidoDao().GerPedVenda(ped, conexao);

                    }
                }

                return ped;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal void updateStatus(long seqPedido, string status)
        {
            try
            {
                using (Connection conexao = new Connection(Connection.CON_SP))
                {
                    OracleCommand comand = new OracleCommand();
                    comand.Parameters.Add(new OracleParameter("pr_seqEdiPedVenda", seqPedido));
                    comand.Parameters.Add(new OracleParameter("pr_status", status));
                    comand.CommandText = "pkg_pedidocliente.p_atualizaStatusPed";
                }


            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal long GerPedVenda(Domain.Pedido.v1.Pedido ped, Connection conexao)
        {
            try
            { 
                OracleCommand comand = new OracleCommand();
                comand.Parameters.Add(new OracleParameter("pr_seqEdiPedVenda", ped.seqPedido));
                comand.Parameters.Add(new OracleParameter("pr_nroempresa", ped.nroEmpresa));

                OracleParameter re_result = new OracleParameter();
                re_result.ParameterName = "v_nropedvenda";
                re_result.OracleDbType = OracleDbType.Int64;
                re_result.Direction = ParameterDirection.Output;
                comand.Parameters.Add(re_result);
                comand.CommandText = "pkg_pedidocliente.p_gerPEdvenda";
                conexao.getResultfromCommand(comand);
                long nroPedVenda = long.Parse(((Oracle.ManagedDataAccess.Types.OracleDecimal)comand.Parameters["v_nropedvenda"].Value).Value.ToString());
                return nroPedVenda;


            }
            catch (Exception e)
            {
                throw e;
            }
        }


        internal List<Utilitarios.Validacao.Erro> validar(Domain.Pedido.v1.Pedido ped)
        {
            List<Utilitarios.Validacao.Erro> erros = new List<Utilitarios.Validacao.Erro>();
            using (Connection conexao = new Connection(Connection.CON_SP))
            {
                erros.AddRange(validarPedido(ped, conexao));
                erros.AddRange(new PedVendaItemDao().validarItens(ped, conexao));
                erros.AddRange(new PedVendaFormaPagtoDao().validarFormaPgto(ped, conexao));
                return erros;
            }
        }


        internal List<Utilitarios.Validacao.Erro> validarPedido(Domain.Pedido.v1.Pedido ped, Connection conexao)
        {
            List<Utilitarios.Validacao.Erro> erros = new List<Utilitarios.Validacao.Erro>();
            try
            {
                OracleCommand comand = new OracleCommand();

                DateTime aux_pr_dtahorretirada = !string.IsNullOrEmpty(ped.dtaHorRetirada) ? DateTime.Parse(ped.dtaHorRetirada) : DateTime.Now;

                comand.Parameters.Add(new OracleParameter("pr_codsistemaafv", ped.codSistemaParceiro));
                comand.Parameters.Add(new OracleParameter("pr_nropedidoafv", ped.nroPedidoAfv));
                comand.Parameters.Add(new OracleParameter("pr_nroempresa", ped.nroEmpresa));
                comand.Parameters.Add(new OracleParameter("pr_nroformapagto", ped.nroFormaPagto));
                comand.Parameters.Add(new OracleParameter("pr_indentregaretira", ped.indEntregaRetira));
                comand.Parameters.Add(new OracleParameter("pr_datahorapedido", ped.DtaPedidoAfv));
                comand.Parameters.Add(new OracleParameter("pr_vlrfrete", ped.vlrFrete));
                comand.Parameters.Add(new OracleParameter("pr_tiporateirofrete", ped.tiporateirofrete));
                comand.Parameters.Add(new OracleParameter("pr_dtaprevinientrega", ped.dtaHoraIniPrevEntrega));
                comand.Parameters.Add(new OracleParameter("pr_dtaprevfimentrega", ped.dtaHoraFimPrevEntrega));
                comand.Parameters.Add(new OracleParameter("pr_seqpessoa", ped.seqPessoa));
                comand.Parameters.Add(new OracleParameter("pr_seqpessoaend", ped.seqEndPessoa > 0 ? (object)ped.seqEndPessoa : DBNull.Value));
                comand.Parameters.Add(new OracleParameter("pr_dtahorretirada", !string.IsNullOrEmpty(ped.dtaHorRetirada) ? (object)aux_pr_dtahorretirada : DBNull.Value));

                OracleParameter re_result = new OracleParameter();
                re_result.ParameterName = "r_result";
                re_result.OracleDbType = OracleDbType.RefCursor;
                re_result.Direction = ParameterDirection.Output;
                comand.Parameters.Add(re_result);
                comand.CommandText = "pkg_pedidocliente.p_validaPedido";
                conexao.getResultfromCommand(comand);
                OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

                return new ReadSQLToObject().DataReaderList(reader, new Erro().GetType()).OfType<Erro>().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        internal Domain.Pedido.v1.Pedido ConsultaPedido(long codSistemaParceiro, long nroPedidoAfv)
        {
            using (Connection conexao = new Connection(Connection.CON_SP))
            {
                try
                {
                    OracleCommand comand = new OracleCommand();
                    comand.Parameters.Add(new OracleParameter("pr_codsistemaafv", codSistemaParceiro));
                    comand.Parameters.Add(new OracleParameter("pr_nropedidoafv", nroPedidoAfv));

                    OracleParameter re_result = new OracleParameter();
                    re_result.ParameterName = "r_result";
                    re_result.OracleDbType = OracleDbType.RefCursor;
                    re_result.Direction = ParameterDirection.Output;
                    comand.Parameters.Add(re_result);
                    comand.CommandText = "pkg_pedidocliente.p_buscapedido";
                    conexao.getResultfromCommand(comand);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)comand.Parameters["r_result"].Value).GetDataReader();

                    Domain.Pedido.v1.Pedido pedido = (Domain.Pedido.v1.Pedido)new ReadSQLToObject().DataReaderObject(reader, new Domain.Pedido.v1.Pedido().GetType());

                    if (pedido.statusPedvenda.ToUpper() == "FATURADO")
                    {
                        pedido.itensFaturados = new PedVendaItemDao().buscaItensFaturados(pedido.nroPedvenda, conexao);
                    }

                    return pedido;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

    }
}
