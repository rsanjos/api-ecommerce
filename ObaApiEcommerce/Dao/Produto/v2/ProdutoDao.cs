﻿using ObaApiEcommerce.Domain.Produto.v2;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Produto.v2
{
    public class ProdutoDao
    {
        public List<ObaApiEcommerce.Domain.Produto.v2.Produto> buscaProduto(DateTime dataBase, int nroEmpresa)
        {

            try
            {

                using (Connection con = new Connection(Connection.CON_SP))
                {
                    OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaProduto_v2");
                    cmd.Parameters.Add(new OracleParameter("pr_database", dataBase));
                    cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroEmpresa));

                    OracleParameter r_result = new OracleParameter();
                    r_result.ParameterName = "r_result";
                    r_result.OracleDbType = OracleDbType.RefCursor;
                    r_result.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(r_result);

                    con.getResultfromCommand(cmd);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();

                    List<ObaApiEcommerce.Domain.Produto.v2.Produto> auxProdutos = new ReadSQLToObject().DataReaderList(reader, new ObaApiEcommerce.Domain.Produto.v2.Produto().GetType()).OfType<ObaApiEcommerce.Domain.Produto.v2.Produto>().ToList();

                    //List<ObaApiEcommerce.Domain.Produto.v2.Produto> produtos = new List<Domain.Produto.v2.Produto>();

                    /*foreach (var item in auxProdutos)
                    {
                        item.codigos = buscaCodigos(item, con);
                        List<urlecommerceimg> urlList = buscaUrlImg(item, con);
                        if (urlList.Count > 0)
                        {
                            //item.urlEcommerceList = item.auxurlEcommerceList.Split(",").ToList();
                            item.urlEcommerceList = urlList.Select(x => x.url).ToList();
                            //if(string.IsNullOrEmpty(item.urlEcommerce))
                            //{
                            //   item.urlEcommerce =  item.urlEcommerce = urlList.Select(c => c.url).First();
                            //}
                            try
                            {
                                item.urlEcommerce = urlList.Where(c => c.indpricipal == "S").Select(c => c.url).First();
                            }catch(Exception)
                            {
                                item.urlEcommerce = urlList.Select(c => c.url).First();
                            }
                        }
                        produtos.Add(item);
                    }*/

                    return auxProdutos;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<urlecommerceimg> buscaUrlImg(Domain.Produto.v2.Produto item, Connection con)
        {
            try
            {
                OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaUrlImg");
                cmd.Parameters.Add(new OracleParameter("pr_seqproduto", item.seqProduto));

                OracleParameter r_result = new OracleParameter();
                r_result.ParameterName = "r_result";
                r_result.OracleDbType = OracleDbType.RefCursor;
                r_result.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(r_result);

                con.getResultfromCommand(cmd);
                OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();

                return new ReadSQLToObject().DataReaderList(reader, new ObaApiEcommerce.Domain.Produto.v2.urlecommerceimg().GetType()).OfType<ObaApiEcommerce.Domain.Produto.v2.urlecommerceimg>().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal Domain.Produto.v2.Produto buscaProduto(long seqProduto, int nroEmpresa)
        {
            try
            {

                using (Connection con = new Connection(Connection.CON_SP))
                {
                    OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaprodutoId");
                    cmd.Parameters.Add(new OracleParameter("pr_seqProduto", seqProduto));
                    cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroEmpresa));

                    OracleParameter r_result = new OracleParameter();
                    r_result.ParameterName = "r_result";
                    r_result.OracleDbType = OracleDbType.RefCursor;
                    r_result.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(r_result);

                    con.getResultfromCommand(cmd);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();

                    ObaApiEcommerce.Domain.Produto.v2.Produto produto = (ObaApiEcommerce.Domain.Produto.v2.Produto)new ReadSQLToObject().DataReaderObject(reader, new ObaApiEcommerce.Domain.Produto.v2.Produto().GetType());

                    //produto.codigos = buscaCodigos(produto, con);

                    if (!string.IsNullOrWhiteSpace(produto.auxurlEcommerceList))
                    {
                        produto.urlEcommerceList = produto.auxurlEcommerceList.Split(",").ToList();
                    }

                    return produto;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private List<Domain.Produto.v2.CodAcesso> buscaCodigos(Domain.Produto.v2.Produto prod, Connection con)
        {
            try
            {
                OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaCodAcesso");
                cmd.Parameters.Add(new OracleParameter("pr_seqproduto", prod.seqProduto));
                cmd.Parameters.Add(new OracleParameter("pr_nroempresa", prod.nroEmpresa));

                OracleParameter r_result = new OracleParameter();
                r_result.ParameterName = "r_result";
                r_result.OracleDbType = OracleDbType.RefCursor;
                r_result.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(r_result);

                con.getResultfromCommand(cmd);
                OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();

                return new ReadSQLToObject().DataReaderList(reader, new ObaApiEcommerce.Domain.Produto.v2.CodAcesso().GetType()).OfType<ObaApiEcommerce.Domain.Produto.v2.CodAcesso>().ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        //public EstoqueProduto BuscaEstqProduto(long seqProduto, int nroEmpresa)
        //{
        //    EstoqueProduto model = new EstoqueProduto();
        //    try
        //    {
        //        using (Connection con = new Connection(Connection.CON_SP))
        //        {
        //            OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaEstoque");
        //            cmd.CommandTimeout = 60;
        //            cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroEmpresa));
        //            cmd.Parameters.Add(new OracleParameter("pr_seqproduto", seqProduto));
        //            cmd.Parameters.Add(new OracleParameter("r_result", OracleDbType.RefCursor, System.Data.ParameterDirection.Output));

        //            con.getResultfromCommand(cmd);
        //            OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();
        //            model = (EstoqueProduto)new ReadSQLToObject().DataReaderObject(reader, model);
        //            return model;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}
        //internal List<Promocao> buscaPromocao(long nroempresa)
        //{
        //    using (Connection con = new Connection(Connection.CON_SP))
        //    {
        //        OracleCommand cmd = new OracleCommand("pkg_produto.p_buscapromocao");
        //        cmd.CommandTimeout = 60;
        //        cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroempresa));
        //        cmd.Parameters.Add(new OracleParameter("r_result", OracleDbType.RefCursor, System.Data.ParameterDirection.Output));

        //        con.getResultfromCommand(cmd);
        //        OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();
        //        List<Promocao> promoList = new ReadSQLToObject().DataReaderList(reader, new Promocao().GetType()).OfType<Promocao>().ToList();
        //        return promoList;
        //    }

        //}

    }
}