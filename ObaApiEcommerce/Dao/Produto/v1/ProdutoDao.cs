﻿using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ObaApiEcommerce.Domain.Produto.v1;

namespace ObaApiEcommerce.Dao.Produto.v1
{
    public class ProdutoDao
    {
        public List<ObaApiEcommerce.Domain.Produto.v1.Produto> buscaProduto(DateTime dataBase, int nroEmpresa)
        {

            try
            {

                using (Connection con = new Connection(Connection.CON_SP))
                {
                    OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaproduto");
                    cmd.Parameters.Add(new OracleParameter("pr_database", dataBase));
                    cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroEmpresa));

                    OracleParameter r_result = new OracleParameter();
                    r_result.ParameterName = "r_result";
                    r_result.OracleDbType = OracleDbType.RefCursor;
                    r_result.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(r_result);

                    con.getResultfromCommand(cmd);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();

                    List<ObaApiEcommerce.Domain.Produto.v1.Produto> auxProdutos = new ReadSQLToObject().DataReaderList(reader, new ObaApiEcommerce.Domain.Produto.v1.Produto().GetType()).OfType<ObaApiEcommerce.Domain.Produto.v1.Produto>().ToList();

                    List<ObaApiEcommerce.Domain.Produto.v1.Produto> produtos = new List<Domain.Produto.v1.Produto>();

                    foreach (var item in auxProdutos)
                    {
                        item.codigos = buscaCodigos(item, con);
                        if (!string.IsNullOrEmpty(item.auxurlEcommerceList))
                        {
                            item.urlEcommerceList = item.auxurlEcommerceList.Split(",").ToList();
                        }
                        produtos.Add(item);
                    }

                    return produtos;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal Domain.Produto.v1.Produto buscaProduto(long seqProduto, int nroEmpresa)
        {
            try
            {

                using (Connection con = new Connection(Connection.CON_SP))
                {
                    OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaprodutoId");
                    cmd.Parameters.Add(new OracleParameter("pr_seqProduto", seqProduto));
                    cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroEmpresa));

                    OracleParameter r_result = new OracleParameter();
                    r_result.ParameterName = "r_result";
                    r_result.OracleDbType = OracleDbType.RefCursor;
                    r_result.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(r_result);

                    con.getResultfromCommand(cmd);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();

                    ObaApiEcommerce.Domain.Produto.v1.Produto produto = (ObaApiEcommerce.Domain.Produto.v1.Produto)new ReadSQLToObject().DataReaderObject(reader, new ObaApiEcommerce.Domain.Produto.v1.Produto().GetType());

                    produto.codigos = buscaCodigos(produto, con);

                    if (!string.IsNullOrWhiteSpace(produto.auxurlEcommerceList))
                    {
                        produto.urlEcommerceList = produto.auxurlEcommerceList.Split(",").ToList();
                    }

                    return produto;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private List<CodAcesso> buscaCodigos(Domain.Produto.v1.Produto prod, Connection con)
        {
            try
            {
                OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaCodAcesso");
                cmd.Parameters.Add(new OracleParameter("pr_seqproduto", prod.seqProduto));
                cmd.Parameters.Add(new OracleParameter("pr_nroempresa", prod.nroEmpresa));

                OracleParameter r_result = new OracleParameter();
                r_result.ParameterName = "r_result";
                r_result.OracleDbType = OracleDbType.RefCursor;
                r_result.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(r_result);

                con.getResultfromCommand(cmd);
                OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();

                return new ReadSQLToObject().DataReaderList(reader, new ObaApiEcommerce.Domain.Produto.v1.CodAcesso().GetType()).OfType<ObaApiEcommerce.Domain.Produto.v1.CodAcesso>().ToList();
            }catch(Exception e)
            {
                throw e;
            }
        }
        public EstoqueProduto BuscaEstqProduto(long seqProduto, int nroEmpresa)
        {
            EstoqueProduto model = new EstoqueProduto();
            try
            {
                using (Connection con = new Connection(Connection.CON_SP))
                {
                    OracleCommand cmd = new OracleCommand("pkg_produto.p_buscaEstoque");
                    cmd.CommandTimeout = 60;
                    cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroEmpresa));
                    cmd.Parameters.Add(new OracleParameter("pr_seqproduto", seqProduto));
                    cmd.Parameters.Add(new OracleParameter("r_result", OracleDbType.RefCursor, System.Data.ParameterDirection.Output));

                    con.getResultfromCommand(cmd);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();
                    model = (EstoqueProduto)new ReadSQLToObject().DataReaderObject(reader, model);
                    return model;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        internal List<Promocao> buscaPromocao(long nroempresa)
        {
            using (Connection con = new Connection(Connection.CON_SP))
            {
                OracleCommand cmd = new OracleCommand("pkg_produto.p_buscapromocao");
                cmd.CommandTimeout = 60;
                cmd.Parameters.Add(new OracleParameter("pr_nroempresa", nroempresa));
                cmd.Parameters.Add(new OracleParameter("r_result", OracleDbType.RefCursor, System.Data.ParameterDirection.Output));

                con.getResultfromCommand(cmd);
                OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();
                List<Promocao> promoList = new ReadSQLToObject().DataReaderList(reader, new Promocao().GetType()).OfType<Promocao>().ToList();
                return promoList;
            }

        }

    }
}