﻿using ObaApiEcommerce.Domain.Produto.v1;
using ObaApiEcommerce.Utilitarios.DataBase;
using ObaApiEcommerce.Utilitarios.ObjectConvert;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObaApiEcommerce.Dao.Produto.v1
{
    public class NutricionaisDao
    {

        internal Nutricionais buscaNutricionais(long seqProduto)
        {

            Nutricionais model = new Nutricionais();
            try
            {
                using (Connection con = new Connection(Connection.CON_SP))
                {
                    OracleCommand cmd = new OracleCommand("pkg_produto.p_buscainfnutric");
                    cmd.CommandTimeout = 60;
                    cmd.Parameters.Add(new OracleParameter("pr_seqproduto", seqProduto));
                    cmd.Parameters.Add(new OracleParameter("r_result", OracleDbType.RefCursor, System.Data.ParameterDirection.Output));

                    con.getResultfromCommand(cmd);
                    OracleDataReader reader = ((Oracle.ManagedDataAccess.Types.OracleRefCursor)cmd.Parameters["r_result"].Value).GetDataReader();
                    model = (Nutricionais)new ReadSQLToObject().DataReaderObject(reader, model);
                    return model;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
